from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models.signals import post_save
from django.dispatch import receiver
#from tinymce import models as tinymce_models
from hitcount.models import HitCountMixin, HitCount
from django.contrib.contenttypes.fields import GenericRelation
from six import python_2_unicode_compatible
from django.urls import reverse


class Author(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_picture = models.ImageField()

    def __str__(self):
        return self.user.username


class Category(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_posted = models.DateTimeField(default=timezone.now)
    content = models.TextField()
    post = models.ForeignKey(
        'Post', related_name='comments', on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

@python_2_unicode_compatible
class Post(models.Model, HitCountMixin):
    title=models.CharField(max_length=100)
    overview = models.TextField()
    #content=tinymce_models.HTMLField(blank=True, null=True)
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    categories = models.ManyToManyField(Category)
    image= models.ImageField(null=True, upload_to='uploads/blogs')
    featured = models.BooleanField(default=False)
    comment_count = models.IntegerField(default = 0)
    objects=models.Manager() 

    
    def date(self):
        return self.date_posted.strftime('%B %d, %Y')
    
    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
            return reverse("post-detail", kwargs={"pk": self.pk})
    
    def get_update_url(self):
        return reverse('post-update', kwargs={
            'pk': self.pk
        })

    def get_delete_url(self):
        return reverse('post-delete', kwargs={
            'pk': self.pk
        })
        
        
    @property
    def get_comments(self):
        return self.comments.all().order_by('-timestamp')

    @property
    def comment_count(self):
        return Comment.objects.filter(post=self).count()
    
    hit_count_generic = GenericRelation(
        HitCount, object_id_field='object_pk',
        related_query_name='hit_count_generic_relation'
    )
    
    def current_hit_count(self):
        return self.hit_count.hits

