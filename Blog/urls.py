from django.urls import path
from Blog.views import (
    PostDetailView, 
    #PostCreateView, 
    BlogView, 
    #PostUpdateView,
    PostDeleteView
    )


urlpatterns = [
    #path('', views.blog, name='blog'),
    path('', BlogView.as_view(), name='blog'),
    path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    #path('create/', PostCreateView.as_view(), name='post-create'),
    #path('post/<id>/', post_detail, name='post-detail'),
    #path('post/<pk>/update/', PostUpdateView.as_view(), name='post-update'),
    path('post/<pk>/delete/', PostDeleteView.as_view(), name='post-delete')

]