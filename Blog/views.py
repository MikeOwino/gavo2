from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from Blog.models import Post
from MainApp.decorators import unauthenticated_user, allowed_users, admin_only
from django.contrib.auth.models import User
from hitcount.views import HitCountDetailView
from django.views.generic import View,CreateView,UpdateView,DeleteView
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
#from .forms import PostForm, CommentForm


'''def blog(request):
    posts=Post.objects.all()
    paginator=Paginator(posts,4)
    page_number = request.GET.get('page')
    try:
        page_obj = paginator.get_page(page_number)
    except PageNotAnInteger:
        page_obj=paginator.page(1)
    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)
    recent_posts=Post.objects.all().order_by('-id')[0:5]
    latest_posts=Post.objects.all().order_by('-id')[0:5]
    context={'page_obj': page_obj,
             'featured_posts':featured_posts, 'recent_posts':recent_posts,'latest_posts':latest_posts, 'title':'blog'}
    return render(request, 'Blog/blog.html', context)'''
    
'''def get_author(user):
    qs = Author.objects.filter(user=user)
    if qs.exists():
        return qs[0]
    return None'''


class BlogView(View):
    
    def get(self, request, *args, **kwargs):
        posts=Post.objects.all().order_by('id')
        paginator=Paginator(posts,4)
        page_number = request.GET.get('page')
        try:
            page_obj = paginator.get_page(page_number)
        except PageNotAnInteger:
            page_obj=paginator.page(1)
        except EmptyPage:
            page_obj = paginator.page(paginator.num_pages)
        recent_posts=Post.objects.all().order_by('-id')[:5]
        latest_posts=Post.objects.all().order_by('-id')[:5]
        featured_posts=posts.filter(featured=True).order_by('-id')[:5]
        popular_posts=posts.order_by('-hit_count_generic__hits')[:5]
        context={'page_obj': page_obj,
             'featured_posts':featured_posts, 
             'recent_posts':recent_posts,
             'latest_posts':latest_posts, 
             'popular_posts':popular_posts,
             'title':'blog'}
        cdn_url = 'https://cdn.gavofoods.co.ke/static/'
        context = {'title': 'blog', 'cdn_url': cdn_url}
        return render(request, 'Blog/blog.html', context)
    

    



class PostDetailView(HitCountDetailView):
    model=Post
    template_name = 'Blog/post_detail.html'
    context_object_name = 'post'
    count_hit = True
    #form_class = CommentForm
    
    '''def get_object(self):
        obj = super().get_object()
        if self.request.user.is_authenticated:
            PostView.objects.get_or_create(
                user=self.request.user,
                post=obj
            )
        return obj'''
    
    
    def get_context_data(self, **kwargs):
        posts=Post.objects.all()
        recent_posts=Post.objects.all().order_by('-id')[:5]
        latest_posts=Post.objects.all().order_by('-id')[:5]
        featured_posts=posts.filter(featured=True).order_by('-id')[:5]
        popular_posts=posts.order_by('-hit_count_generic__hits')[:5]
        context = super().get_context_data(**kwargs)
        context["recent_posts"] =recent_posts
        context["latest_posts"] =latest_posts 
        context["featured_posts"] =featured_posts 
        context["popular_posts"] =popular_posts  
        return context
    '''(self, request, *args, **kwargs):
        posts=Post.objects.all().order_by('id')
        recent_posts=Post.objects.all().order_by('-id')[:5]
        latest_posts=Post.objects.all().order_by('-id')[:5]
        featured_posts=posts.filter(featured=True).order_by('-id')[:5]
        popular_posts=posts.order_by('-hit_count_generic__hits')[:5]
        context={
             'featured_posts':featured_posts, 
             'recent_posts':recent_posts,
             'latest_posts':latest_posts, 
             'popular_posts':popular_posts,
             'title':'blog'}
        

        return context'''
    
'''def post_detail(request, id):
    posts=Post.objects.all()
    recent_posts=Post.objects.all().order_by('-id')[:5]
    latest_posts=Post.objects.all().order_by('-id')[:5]
    featured_posts=posts.filter(featured=True).order_by('-id')[:5]
    popular_posts=posts.order_by('-hit_count_generic__hits')[:5]
    post = get_object_or_404(Post, id=id)
    context={
             'featured_posts':featured_posts, 
             'recent_posts':recent_posts,
             'latest_posts':latest_posts, 
             'popular_posts':popular_posts,
             'title':'blog',
             'post':post
             }
    return render(request, 'Blog/post_detail.html', context)'''


'''class PostCreateView(CreateView):
    model = Post
    template_name = 'Blog/post_create.html'
    form_class = PostForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Create'
        return context

    def form_valid(self, form):
        form.instance.author=self.request.user
        form.save()
        return redirect(reverse("post-detail", kwargs={
            'pk': form.instance.pk
        }))'''


'''class PostUpdateView(UpdateView):
    model = Post
    template_name = 'Blog/post_create.html'
    form_class = PostForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Update'
        return context

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.save()
        return redirect(reverse("post-detail", kwargs={
            'pk': form.instance.pk
        }))'''


class PostDeleteView(DeleteView):
    model = Post
    success_url = '/blog'
    template_name = 'Blog/post_confirm_delete.html'