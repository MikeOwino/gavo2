from django.contrib import admin
from .models import (Customer,
                     Flour_product,
                     Baked_product,
                     Tag,
                     Order,
                     )


admin.site.register(Customer)
admin.site.register(Flour_product)
admin.site.register(Tag)
admin.site.register(Order)
admin.site.register(Baked_product)
