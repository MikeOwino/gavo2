from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models.signals import post_save
from django.dispatch import receiver
#from tinymce import models as tinymce_models

class Customer(models.Model):
	user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
	name = models.CharField(max_length=200, null=True)
	phone = models.CharField(max_length=200, null=True)
	email = models.CharField(max_length=200, null=True)
	profile_pic = models.ImageField(default="profile1.png", null=True, blank=True)
	date_created = models.DateTimeField(auto_now_add=True, null=True)

	def __str__(self):
		return self.name


class Tag(models.Model):
    name = models.CharField(max_length=200, null=True)
    
    
    def __str__(self):
         return self.name


class Flour_product(models.Model):
    CATEGORY = (
			('In stock', 'In stock'),
			('Out of Stock', 'Out of Stock'),
			) 
    name = models.CharField(max_length=200, null=True)
    price = models.FloatField(null=True)
    category = models.CharField(max_length=200, null=True, choices=CATEGORY)
    description = models.CharField(max_length=200, blank=True, null=True)
    nutritional_value =models.CharField(max_length=200, blank=True, null=True)
    delivery = models.CharField(max_length=200, blank=True, null=True)
    image = models.ImageField(upload_to = 'uploads/', null=True)
    #tags = models.ManyToManyField(Tag)
    
    
    def __str__(self):
     return self.name

class Baked_product(models.Model):
    CATEGORY = (
			('In stock', 'In stock'),
			('Out of Stock', 'Out of Stock'),
			) 
    name = models.CharField(max_length=200, null=True)
    price = models.FloatField(null=True)
    category = models.CharField(max_length=200, null=True, choices=CATEGORY)
    description = models.CharField(max_length=2000, null=True, blank=True)
    image = models.ImageField(upload_to = 'uploads/', null=True)
    tags = models.ManyToManyField(Tag)
    
    
    def __str__(self):
     return self.name

    
    def __str__(self):
     return self.name
 
 
class Order(models.Model):
    STATUS = (
			('Pending', 'Pending'),
			('Delivered', 'Delivered'),
			)
    customer = models.ForeignKey(Customer, null=True, on_delete= models.SET_NULL)
    product = models.ForeignKey(Flour_product, null=True, on_delete= models.SET_NULL)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    status = models.CharField(max_length=200, null=True, choices=STATUS)
    Message = models.CharField(max_length=1000, null=True)
    
    def __str__(self):
     return self.product.name