from django.urls import path
from MainApp import views
from .views import BakedProductDetailView,FlourProductDetailView



urlpatterns = [
    path('', views.home, name='index-page'),
    path('flours/', views.flour_products, name='flour-products'),
    path('baked/', views.baked_products, name='baked-products'),
    path('flourproduct/<int:pk>/', FlourProductDetailView.as_view(), name='flour-product-detail'),
    path('bakedproduct/<int:pk>/', BakedProductDetailView.as_view(), name='baked-product-detail'),
    path('cafe/', views.cafe, name="cafe"),
    path('contact/', views.contact, name='contact'),
    path('about/', views.about, name='about'),
    path('team/', views.team, name='team'),
    path('services/', views.services, name='services'),
    path('gallery/', views.gallery, name='gallery'),
    path('careers/', views.careers, name='careers'),
    path('signup/', views.signup, name="signup"),
    path('login/', views.Person_login, name="login"),
    path('logout/', views.Person_logout, name="logout"),
    path('contact_form/', views.contact, name='contact-form')  
    #path('comingsoon', views.comingsoon, name='coming')
]