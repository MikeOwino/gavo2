from django.shortcuts import render, redirect 
from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from MainApp.models import Customer, Flour_product,Baked_product
from Blog.models import Post
from .forms import CreateUserForm
from .decorators import unauthenticated_user, allowed_users, admin_only
from django.contrib.auth.models import User
from django.views.generic import ListView,DetailView,CreateView,UpdateView,DeleteView
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.core.mail import send_mail
from django.http import JsonResponse



#@unauthenticated_user
def Person_login(request):

	if request.method == 'POST':
		username = request.POST.get('username')
		password =request.POST.get('password')

		user = authenticate(request, username=username, password=password)

		if user is not None:
			login(request, user)
			return redirect('index-page')
		else:
			messages.info(request, 'Username OR password is incorrect')

	context = {}
	return render(request, 'MainApp/login.html', context)

def Person_logout(request):
    logout(request)
    return redirect('index-page')

def signup(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            username = form.save()
            username = form.cleaned_data.get('username')
            m=", thanks for joining us. You can now login."
            messages.success(request, 'Hi ' + username + m)
            return redirect('login')
		
    context = {'form':form}
    return render(request, 'MainApp/signup.html', context)

def home(request):
    products=Flour_product.objects.all()
    posts = Post.objects.all().order_by('-id')[:2]
    data = {}
    if request.method == 'POST':
        name = request.POST.get('name')
        phone = request.POST.get('phone')
        email = request.POST.get('email')
        subject = request.POST.get('subject')
        message = request.POST.get('message')
        
        data = {
            'name': name,
            'phone': phone,
            'email': email,
            'subject': subject,
            'message': message
        }
        message = '''
        New message:{}
        From:{}
        Phone:{}
        '''.format(data['message'], data['email'], data['phone'])
        send_mail(data['subject'], message,'', ['gavopumgfree@gmail.com'])

    cdn_url = 'https://cdn.gavofoods.co.ke/static/'
    context = {
        'posts': posts,
        'cdn_url': cdn_url,
        'products':products,'title':'products',
    }
    return render(request, 'MainApp/index.html', context)


def flour_products(request):
    products=Flour_product.objects.all()
    context={'products':products,'title':'products'}
    return render(request, 'MainApp/flour_products.html', context)

class FlourProductDetailView(DetailView):
    model=Flour_product

def baked_products(request):
    products=Baked_product.objects.all()
    context={'products':products,'title':'products'}
    return render(request, 'MainApp/baked_products.html', context)


class BakedProductDetailView(DetailView):
    model=Baked_product

def contact(request):
    cdn_url = 'https://cdn.gavofoods.co.ke/static/'
    context = {'title': 'contact', 'cdn_url': cdn_url}
    if request.method == 'POST':
        name = request.POST.get('name')
        phone = request.POST.get('phone')
        email = request.POST.get('email')
        subject = request.POST.get('subject')
        message = request.POST.get('message')

        data = {
            'name': name,
            'phone': phone,
            'email': email,
            'subject': subject,
            'message': message
        }
        message = '''
        
        New message:{}
        From:{}
        Phone:{}
        '''.format(data['message'], data['email'], data['phone'])
        send_mail(data['subject'], message, '', ['gavopumgfree@gmail.com'])

    return render(request, 'MainApp/contact.html', context)


def about(request):
    cdn_url = 'https://cdn.gavofoods.co.ke/static/'
    context = {'title': 'about', 'cdn_url': cdn_url}
    return render(request, 'MainApp/about.html', context)
    # return render(request, 'MainApp/about.html', {'title':'about'})

def team(request):
    cdn_url = 'https://cdn.gavofoods.co.ke/static/'
    context = {'title': 'team', 'cdn_url': cdn_url}
    return render(request, 'MainApp/team.html', context)
    # return render(request, 'MainApp/about.html', {'title':'about'})

def services(request):
    cdn_url = 'https://cdn.gavofoods.co.ke/static/'
    context = {'title': 'services', 'cdn_url': cdn_url}
    return render(request, 'MainApp/services.html', context)
    # return render(request, 'MainApp/about.html', {'title':'about'})

def gallery(request):
    cdn_url = 'https://cdn.gavofoods.co.ke/static/'
    context = {'title': 'gallery', 'cdn_url': cdn_url}
    return render(request, 'MainApp/gallery.html', context)
    # return render(request, 'MainApp/about.html', {'title':'about'})

def careers(request):
    cdn_url = 'https://cdn.gavofoods.co.ke/static/'
    context = {'title': 'careers', 'cdn_url': cdn_url}
    return render(request, 'MainApp/careers.html', context)
    # return render(request, 'MainApp/about.html', {'title':'about'})

def cafe(request):
    cdn_url = 'https://cdn.gavofoods.co.ke/static/'
    context = {'title': 'cafe', 'cdn_url': cdn_url}
    return render(request, 'MainApp/cafe.html', context)
    # return render(request, 'MainApp/cafe.html',{'title':'cafe'} )



'''def comingsoon(request):
    return render(request, 'MainApp/comingsoon.html')'''

"""def contact(request):
    if request.is_ajax():
        name = request.POST.get('name')
        email = request.POST.get('email')
        subject = request.POST.get('subject')
        message = request.POST.get('message')
        data={
        'name': name,
        'email': email,
        'subject': subject,
        'message': message
        }
        message='''
        
        New message:{}
        From:{}
        Phone:{}
        '''.format(data['message'], data['email'], data['phone'])
        send_mail(data['subject'], message,'', ['gavopumgfree@gmail.com'])
        return 'True Data'
    else:
        return 'False data'
    """

